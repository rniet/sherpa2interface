#################
# CONFIGURATION #
#################
nEvts = 1
EventType = '11144001'
#################
#################
from Gauss.Configuration import *

#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber        = 1082

#--Number of events
LHCbApp().EvtMax = nEvts

idFile = 'Gauss-'+EventType
HistogramPersistencySvc().OutputFile = idFile+'-histos.root'
OutputStream("GaussTape").Output = "DATAFILE='PFN:%s.xgen' TYP='POOL_ROOTTREE' OPT='RECREATE'"%idFile

from Configurables import LHCbApp
LHCbApp().DDDBtag   = "Sim08-20130503-1"
LHCbApp().CondDBtag = "Sim08-20130503-1-vc-md100"

#-- import options
importOptions('./'+EventType+'_Sherpa.py')
importOptions('$APPCONFIGOPTS/Conditions/Sim08a-2012-md100.py')
importOptions('$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-md100-2012-nu2.5.py')
importOptions('$APPCONFIGOPTS/Gauss/xgen.py')

from Configurables import WriteHepMCAsciiFile
def doMyChanges():
  WriteHepMCAsciiFile().Output = "test.txt"
  ApplicationMgr().TopAlg += [ WriteHepMCAsciiFile()]

appendPostConfigAction(doMyChanges)
