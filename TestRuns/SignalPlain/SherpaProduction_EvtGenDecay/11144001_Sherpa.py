# file /afs/cern.ch/lhcb/software/releases/DBASE/Gen/DecFiles/v27r8/options/11144001.py generated: Tue, 02 Jul 2013 17:16:36
#
# Event Type: 11144001
#
# ASCII decay Descriptor: {[[B0]nos -> (J/psi(1S) -> mu+ mu- {,gamma} {,gamma}) (K*(892)0 -> K+ pi-)]cc, [[B0]os -> (J/psi(1S) -> mu+ mu- {,gamma} {,gamma}) (K*(892)~0 -> K- pi+)]cc}
#

#modified for testing sherpa interface to gauss

from Configurables import Generation
Generation().EventType = 11144001
Generation().SampleGenerationTool = "SignalPlain"

from Configurables import SignalPlain
Generation().addTool(SignalPlain, name='SignalPlain')
Generation().SignalPlain.ProductionTool = "SherpaProduction"
Generation().SignalPlain.OutputLevel = 2
Generation().SignalPlain.CutTool = ""
Generation().SignalPlain.SignalPIDList = [511,-511]

from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Bd_JpsiKst,mm=DecProdCut.dec"
