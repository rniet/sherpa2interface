# file /afs/cern.ch/lhcb/software/releases/DBASE/Gen/DecFiles/v27r8/options/11144001.py generated: Tue, 02 Jul 2013 17:16:36
#
# Event Type: 11144001
#
# ASCII decay Descriptor: {[[B0]nos -> (J/psi(1S) -> mu+ mu- {,gamma} {,gamma}) (K*(892)0 -> K+ pi-)]cc, [[B0]os -> (J/psi(1S) -> mu+ mu- {,gamma} {,gamma}) (K*(892)~0 -> K- pi+)]cc}
#

#modified for testing sherpa interface to gauss

from Configurables import Generation
Generation().EventType = 11144001
Generation().SampleGenerationTool = "SignalPlain"
Generation().OutputLevel = 2
Generation().DecayTool = "SherpaDecay"

from Configurables import SignalPlain
from Configurables import SherpaDecay
from Configurables import ToolSvc

Generation().addTool(SignalPlain, name='SignalPlain')
Generation().SignalPlain.ProductionTool = "PythiaProduction"
Generation().SignalPlain.DecayTool = "SherpaDecay"
Generation().DecayTool = "SherpaDecay"

Generation().SignalPlain.OutputLevel = 2
Generation().SignalPlain.CutTool = ""
Generation().SignalPlain.SignalPIDList = [511,-511]

# This is quite ugly - ToolSvc owns DecayTool, so have to configure it this way
ToolSvc().addTool( SherpaDecay )
ToolSvc().SherpaDecay.OutputLevel = 2
