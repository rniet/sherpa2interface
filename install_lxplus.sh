CMT_USER_PATH=/afs/cern.ch/user/r/rniet/cmtuser/
GAUSS_VERSION=Gauss_v48r0p1

INTERFACE_GIT_PATH=/afs/cern.ch/user/r/rniet/repos/sherpa2interface/
INTERFACE_CMT_PATH=$CMT_USER_PATH$GAUSS_VERSION

mkdir -p $INTERFACE_CMT_PATH/Gen/SherpaDatFiles/SteeringDats/ProdToolDats
mkdir -p $INTERFACE_CMT_PATH/Gen/SherpaDatFiles/SteeringDats/DecayToolDats

cp $INTERFACE_GIT_PATH/SherpaDatFiles/SteeringDats/ProdToolDats/Run.dat $INTERFACE_CMT_PATH/Gen/SherpaDatFiles/SteeringDats/ProdToolDats/
cp $INTERFACE_GIT_PATH/SherpaDatFiles/SteeringDats/DecayToolDats/Run.dat $INTERFACE_CMT_PATH/Gen/SherpaDatFiles/SteeringDats/DecayToolDats/

#cp $INTERFACE_GIT_PATH/LbSherpa/SherpaRun/LHC/Run.dat $INTERFACE_CMT_PATH/Gen/LbSherpa/SherpaRun/LHC/

cp $INTERFACE_GIT_PATH/LbSherpa/src/*.{cpp,h} $INTERFACE_CMT_PATH/Gen/LbSherpa/src/
cp $INTERFACE_GIT_PATH/LbSherpa/cmt/requirements_lxplus $INTERFACE_CMT_PATH/Gen/LbSherpa/cmt/requirements
cp $INTERFACE_GIT_PATH/sherpa/cmt/requirements_lxplus $INTERFACE_CMT_PATH/Gen/sherpa/cmt/requirements
