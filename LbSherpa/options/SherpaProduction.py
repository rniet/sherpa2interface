#Append this for using Sherpa as the ProductionTool in the generation phase of Gauss
#Outputlevel 2 (DEBUG) since SherpaInterface is still under development

from Configurables import Generation, Special, SherpaProduction 

gen = Generation()
gen.addTool(Special, name = "Special" )
gen.Special.ProductionTool = "SherpaProduction"
gen.Special.addTool(SherpaProduction, name = "SherpaProduction")
gen.Special.SherpaProduction.OutputLevel = 2

#gen.PileUpTool = "FixedLuminosityForRareProcess"
