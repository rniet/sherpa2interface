############################################################################
# 2009-07-29 jwishahi
#
# File for running Sherpa as DecayTool with Pythia as ProductionTool
# Syntax: GaudiRun SherpaDecay.py
############################################################################

from Gaudi.Configuration import *
from Gauss.Configuration import *

############################################################################
# Beam configuration
############################################################################

#--Set the L/nbb, total cross section and revolution frequency and configure
#--the pileup tool
Gauss().Luminosity        = 0.116*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().CrossingRate      = 11.245*SystemOfUnits.kilohertz
Gauss().TotalCrossSection = 97.2*SystemOfUnits.millibarn

#--Set the luminous region for colliding beams and beam gas and configure
#--the corresponding vertex smearing tools, the choice of the tools is done
#--by the event type
Gauss().InteractionSize = [ 0.027*SystemOfUnits.mm, 0.027*SystemOfUnits.mm,
                            3.82*SystemOfUnits.cm ]
Gauss().BeamSize        = [ 0.038*SystemOfUnits.mm, 0.038*SystemOfUnits.mm ]

#--Set the energy of the beam,
#--the half effective crossing angle (in LHCb coordinate system),
#--beta* and emittance
Gauss().BeamMomentum      = 5.0*SystemOfUnits.TeV
Gauss().BeamCrossingAngle = 0.329*SystemOfUnits.mrad
Gauss().BeamEmittance     = 0.704*(10**(-9))*SystemOfUnits.rad*SystemOfUnits.m
Gauss().BeamBetaStar      = 2.0*SystemOfUnits.m

#--Starting time, to identify beam conditions, all events will have the same
ec = EventClockSvc()
ec.addTool(FakeEventTime(), name="EventTimeDecoder")
ec.EventTimeDecoder.StartTime = 1.0*SystemOfUnits.ms
ec.EventTimeDecoder.TimeStep  = 0.0



############################################################################
# Database information
############################################################################

from Configurables import LHCbApp

#vc = Velo Closed, md = magnetic field "down"

### Load my own DDDB
from Configurables import CondDB
dddb = CondDB()
dddb.addLayer(dbFile = "$HOME/MyDDDBs/SherpaDDDB.db", dbName = "DDDB")

LHCbApp().CondDBtag = "sim-20090402-vc-md100"
LHCbApp().DDDBtag   = "MC09-20090602"


############################################################################
# GeneratorPhase options
############################################################################
Gauss().GenStandAlone = True


############################################################################
# Event Options
############################################################################
importOptions('$SHERPADATFILESROOT/options/PythiaSherpa/10000000.opts')


############################################################################
# Generator options 
############################################################################
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber        = 10

LHCbApp().EvtMax          = 10


