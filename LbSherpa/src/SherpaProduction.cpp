// $Header: /local/reps/lhcb/Gen/LbSherpa/src/SherpaProduction.cpp,v 1.1.1.1 2009/02/26 16:54:56 jwishahi Exp $

// Include files


// from Gaudi
#include "GaudiKernel/System.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "Kernel/ParticleProperty.h"
#include "GaudiKernel/PhysicalConstants.h"

//#include "GaudiKernel/IParticlePropertySvc.h"
//#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"

// from GeneratorsModule
#include "Generators/IBeamTool.h"
#include "Generators/RandomForGenerator.h"
#include "Generators/IProductionTool.h"

// from Event
#include "Event/GenCollision.h"

// HepMC
#include "HepMC/GenEvent.h"
#include "HepMC/GenCrossSection.h"

// Sherpa Header
#include "SHERPA/Main/Sherpa.H"
#include "SHERPA/Single_Events/Event_Handler.H"
#include "SHERPA/Initialization/Initialization_Handler.H"

#include "SHERPA/Single_Events/Signal_Processes.H"
#include "SHERPA/Single_Events/Hard_Decays.H"
#include "SHERPA/Single_Events/Minimum_Bias.H"
#include "SHERPA/Single_Events/Multiple_Interactions.H"
#include "SHERPA/Single_Events/Jet_Evolution.H"
#include "SHERPA/Single_Events/Signal_Process_FS_QED_Correction.H"
#include "SHERPA/Single_Events/Beam_Remnants.H"
#include "SHERPA/Single_Events/Hadronization.H"
#include "SHERPA/Single_Events/Hadron_Decays.H"
#include "SHERPA/PerturbativePhysics/Hard_Decay_Handler.H"

#include "SHERPA/Tools/Definitions.H"
#include "SHERPA/Tools/HepMC2_Interface.H"

#include "ATOOLS/Org/Exception.H"
#include "ATOOLS/Org/MyStrStream.H"
#include "ATOOLS/Phys/Blob.H"

// local
#include "SherpaProduction.h"

#include <iostream>

// Random Generator, has to be global so that Sherpa can access it.
// Had to give it a different name than the one in SherpaDecay to avoid global variable clashes.
// Is this really a problem?? It's definitely no nice solution.
Rndm::Numbers m_gaudiRNGProd;

//-----------------------------------------------------------------------------
//  Implementation file for class: SherpaProduction
//
//  2015-02-03 Ramon Niet
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( SherpaProduction );

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SherpaProduction::SherpaProduction( const std::string& type,
                                    const std::string& name,
                                    const IInterface* parent )
  : GaudiTool ( type, name , parent ), m_initialised(false) {
  debug() << "=== LbSherpa: SherpaProduction::SherpaProduction" << endmsg;
  declareInterface< IProductionTool >( this );
  declareProperty( "BeamToolName", m_beamToolName = "CollidingBeams" );
  declareProperty( "RUNDATA",      m_RundatPath   = System::getEnv("SHERPADATFILESROOT" )+"/SteeringDats/ProdToolDats/Run.dat", "Location of the SHERPA Run.dat steering file" );
  declareProperty( "DoTheDecays",  m_doTheDecays  = false );

}

//=============================================================================
// Destructor 
//=============================================================================
SherpaProduction::~SherpaProduction( ) { }

//=============================================================================
// Initialize method
//=============================================================================
StatusCode SherpaProduction::initialize( ) {
  debug()  << "=== LbSherpa: SherpaProduction::initialize" << endmsg;
  always() << "=============================================================" << endmsg;
  always() << "Using as production engine  " << this->type() << endmsg;
  always() << "=============================================================" << endmsg;

  //Initializing Gaudi tool
  StatusCode sc = GaudiTool::initialize( );
  if ( sc.isFailure() ) return sc;

  //Initializing the beam tool and setting beam properties in Sherpa
  m_beamTool = tool< IBeamTool >( m_beamToolName , this );
  Gaudi::XYZVector pBeam1, pBeam2;
  m_beamTool->getMeanBeams( pBeam1, pBeam2 );
  pBeam1 /= Gaudi::Units::GeV;
  pBeam2 /= Gaudi::Units::GeV;
  double m_eng1 = sqrt(pBeam1.Dot(pBeam1));      // beam1 energy in GeV
  double m_eng2 = sqrt(pBeam2.Dot(pBeam2));      // beam2 energy in GeV

  //Setting Sherpa properties
  m_parameters["RUNDATA"]          = m_RundatPath;
  m_parameters["EXTERNAL_RNG"]     = "LHCb_RNG";
  m_parameters["EVENTS"]           = "1";
  m_parameters["MASS_SMEARING"]    = "0";
  m_parameters["RESULT_DIRECTORY"] = "Results/";
  m_parameters["BEAM_1"]           = "2212";
  m_parameters["BEAM_2"]           = "2212";
  m_parameters["BEAM_ENERGY_1"]    = std::to_string(m_eng1);
  m_parameters["BEAM_ENERGY_2"]    = std::to_string(m_eng2);

  m_parameters["SOFT_MASS_SMEARING"] = "2"; //critical
  m_parameters["HARD_MASS_SMEARING"] = "2"; //critical

  // Initializing the random generator
  IRndmGenSvc* randSvc( 0 );
  try {
    randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true );
  } catch ( const GaudiException & exc ) {
    Exception( "RndmGenSvc not found to initialize Sherpa random engine" );
  }
  m_gaudiRNGProd.initialize( randSvc , Rndm::Flat( 0 , 1 ) );

  return StatusCode::SUCCESS;
}

///=============================================================================
/// Part specific to generator initialization
///=============================================================================
StatusCode SherpaProduction::initializeGenerator( ) {
  debug() << "=== LbSherpa: initializeGenerator" << endmsg;
  //Setup sherpa arguments                                                      
  std::vector<std::string> argv;
  argv.push_back("dummy");
  for (std::map<std::string,std::string>::const_iterator it =m_parameters.begin(); it !=m_parameters.end(); ++it) {
    std::string command = (it->first + "=" + it->second);
    argv.push_back(command);
    debug() << "=== LbSherpa: initializeGenerator " << command << endmsg;
  }
  std::vector<char*> argc;
  for (std::vector<char*>::size_type i = 0; i != argv.size(); ++i) {
    argc.push_back( const_cast<char*>(argv[i].c_str() ) );
  }
  debug() << "=== LbSherpa: Initializing Sherpa Run" << endmsg;
  p_generator = new SHERPA::Sherpa();
  try {
    p_generator->InitializeTheRun(argc.size(), &argc[0]);
    p_generator->InitializeTheEventHandler();
  } catch (ATOOLS::Exception Sherpaexception) {
    msg_Error() << Sherpaexception << std::endl;
    exit(1);
  }
  debug() << "=== LbSherpa: Run initialized" << endmsg;
  m_initialised = true;
  return StatusCode::SUCCESS;
}


//=============================================================================
//   Function called to generate one event with Sherpa
//=============================================================================
StatusCode SherpaProduction::generateEvent( HepMC::GenEvent *theEvent, 
                                            LHCb::GenCollision *theCollision ) {
  debug() << "=== LbSherpa: generateEvent" << endmsg;
  if (!m_initialised) {
    initializeGenerator();
    m_initialised=true;
  }
  try {
    p_generator->GenerateOneEvent();
  } catch (ATOOLS::Exception Sherpaexception) {
    msg_Error() << Sherpaexception << std::endl;
    exit(1);
  }

  p_generator->FillHepMCEvent(*theEvent);
  HepMC::GenCrossSection crosssection;
  crosssection.set_cross_section(p_generator->GetEventHandler()->TotalXS(), p_generator->GetEventHandler()->TotalErr());
  theEvent->set_cross_section(crosssection);
  
  // This converts GeV (Sherpa) to MeV (Gaudi)
  for ( HepMC::GenEvent::particle_iterator p = theEvent->particles_begin(); p != theEvent->particles_end(); ++p ) {
    HepMC::FourVector scaled_momentum((*p)->momentum().px() * Gaudi::Units::GeV,
                                      (*p)->momentum().py() * Gaudi::Units::GeV,
                                      (*p)->momentum().pz() * Gaudi::Units::GeV,
                                      (*p)->momentum().e()  * Gaudi::Units::GeV);
    (*p)->set_momentum(scaled_momentum);
    (*p)->set_generated_mass((*p)->generated_mass() * Gaudi::Units::GeV);
  }

  hardProcessInfo(theCollision);
  return StatusCode::SUCCESS;
}

//=============================================================================
// TRUE if the particle is a special particle which must not be modified
//=============================================================================
bool SherpaProduction::isSpecialParticle(const LHCb::ParticleProperty* thePP ) const {
  debug() << "=== LbSherpa: isSpecialParticle - PDGId " << thePP->pdgID() << endmsg;
  int abspdgID    = abs(thePP->pdgID().pid());
  double lifetime = thePP->lifetime();
  if ( abspdgID <= 100 || abspdgID > 9900000 )
    return true;
  else if ( lifetime == 0.0 || lifetime > 1.e15)
    return true;
  else if ( abspdgID==311 || abspdgID==310 || abspdgID==130 || abspdgID==5314 || abspdgID==5324 || abspdgID==541 )
    return true;
  else
    return false;
}

//=============================================================================
// Set stable the given particle in Sherpa
//=============================================================================
void SherpaProduction::setStable( const LHCb::ParticleProperty* thePP ) {
  debug() << "=== LbSherpa: setStable - PDGId " << thePP->pdgID() << endmsg;
  
  if(m_doTheDecays) {
    debug() << "=== LbSherpa: setStable - configured to DoTheDecays - not setting stable" << endmsg;
    return;
  }

  if ( thePP->pdgID().pid()<0 ) return;
  unsigned long int pdg_id = thePP->pdgID().pid();
  unsigned long int sherpa_id = ATOOLS::Flavour::PdgToSherpa(pdg_id);

  m_parameters["STABLE["+ATOOLS::ToString(sherpa_id)+"]"] = "1";
}

//=============================================================================
// Update particle properties
//=============================================================================
void SherpaProduction::updateParticleProperties( const LHCb::ParticleProperty * thePP ) {
  unsigned long int pdg_id = abs(thePP->pdgID().pid());
  unsigned long int sherpa_id = ATOOLS::Flavour::PdgToSherpa(pdg_id);
  debug() << "=== LbSherpa: updateParticleProperties called for PDGid " << pdg_id << endmsg;
  double gaudiwidth = ( Gaudi::Units::hbarc / ( thePP -> lifetime() * Gaudi::Units::c_light ) );
  debug() << "=== LbSherpa: updateParticleProperties updateing " << thePP->name() << " sherpa_id " << sherpa_id << endmsg;
  debug() << "=== LbSherpa: updateParticleProperties MASS: " << thePP->mass() / Gaudi::Units::GeV << " WIDTH: " << gaudiwidth / Gaudi::Units::GeV << endmsg;
  m_parameters["MASS[" + ATOOLS::ToString(sherpa_id) + "]"] = ATOOLS::ToString(thePP->mass() / Gaudi::Units::GeV);
  m_parameters["WIDTH["+ ATOOLS::ToString(sherpa_id) + "]"] = ATOOLS::ToString(gaudiwidth / Gaudi::Units::GeV);
}

//=============================================================================
// Retrieve the Hard scatter information
//=============================================================================
void SherpaProduction::hardProcessInfo( LHCb::GenCollision* /*theCollision*/) {
  debug() << "=== LbSherpa: hardProcessInfo (NOT IMPLEMENTED)" << endmsg;
  // ATOOLS::Blob_List* blobs = p_generator->GetEventHandler()->GetBlobs();
  // ATOOLS::Blob *blob(blobs->FindFirst(ATOOLS::btp::Signal_Process));
  // std::cout << "The Signal Blob: " << std::endl;
  // std::cout <<  *blob << std::endl;
  // Do you really have to do this manually or is there an interface in SHERPA?
} 

//=============================================================================
// Finalize method
//=============================================================================
StatusCode SherpaProduction::finalize( ) {
  debug() << "=== LbSherpa: finalize" << endmsg;
  m_gaudiRNGProd.finalize( );
  return GaudiTool::finalize( );
}  


//=============================================================================
// Turn on fragmentation in Sherpa
//=============================================================================
void SherpaProduction::turnOnFragmentation( ){
  debug() << "=== LbSherpa: turnOnFragmentation" << endmsg;

  SHERPA::Event_Handler *p_eventhandler         = p_generator->GetEventHandler();
  SHERPA::Initialization_Handler *p_inithandler = p_generator->GetInitHandler();

  p_eventhandler->AddEventPhase(new SHERPA::Hadronization(p_inithandler->GetFragmentationHandler()));
  p_eventhandler->AddEventPhase(new SHERPA::Hadron_Decays(p_inithandler->GetHDHandler()));
  // for debug purposes:
  // p_eventhandler->PrintGenericEventStructure();
}

//=============================================================================
// Turn off fragmentation in Sherpa
//=============================================================================
void SherpaProduction::turnOffFragmentation( ){
  debug() << "=== LbSherpa: turnOffFragmentation" << endmsg;

  SHERPA::Event_Handler *p_eventhandler         = p_generator->GetEventHandler();
  SHERPA::Initialization_Handler *p_inithandler = p_generator->GetInitHandler();
  
  p_eventhandler->EmptyEventPhases();
 
  p_eventhandler->AddEventPhase(new SHERPA::Signal_Processes(p_inithandler->GetMatrixElementHandler()));
  p_eventhandler->AddEventPhase(new SHERPA::Hard_Decays(p_inithandler->GetHardDecayHandler()));
  p_eventhandler->AddEventPhase(new SHERPA::Jet_Evolution(p_inithandler->GetMatrixElementHandler(),
                                                          p_inithandler->GetHardDecayHandler(),
                                                          p_inithandler->GetHDHandler(),
                                                          p_inithandler->GetMIHandler(),
                                                          p_inithandler->GetSoftCollisionHandler(),
                                                          p_inithandler->GetShowerHandlers()));
  p_eventhandler->AddEventPhase(new SHERPA::Signal_Process_FS_QED_Correction(p_inithandler->GetMatrixElementHandler(),
                                                                             p_inithandler->GetSoftPhotonHandler()));
  p_eventhandler->AddEventPhase(new SHERPA::Multiple_Interactions(p_inithandler->GetMIHandler()));
  p_eventhandler->AddEventPhase(new SHERPA::Minimum_Bias(p_inithandler->GetSoftCollisionHandler()));
  p_eventhandler->AddEventPhase(new SHERPA::Beam_Remnants(p_inithandler->GetBeamRemnantHandler()));
  // for debug purposes:
  // p_eventhandler->PrintGenericEventStructure();
}

//=============================================================================
// Save parton event
//=============================================================================
void SherpaProduction::savePartonEvent( HepMC::GenEvent * /* theEvent */ ) {
  debug() << "=== LbSherpa: savePartonEvent" << endmsg;
  SHERPA::Event_Handler *p_eventhandler = p_generator->GetEventHandler();
  m_event.Clear();
  m_event = p_eventhandler->GetBlobs()->Copy();
}

//=============================================================================
// Load parton event
//=============================================================================
void SherpaProduction::retrievePartonEvent( HepMC::GenEvent * /* theEvent */ ) {
  debug() << "=== LbSherpa: retrievePartonEvent" << endmsg;
  SHERPA::Event_Handler *p_eventhandler = p_generator->GetEventHandler();
  p_eventhandler->GetBlobs()->Clear();
  *p_eventhandler->GetBlobs() = m_event.Copy();
}

//=============================================================================
// Hadronize Sherpa event
//=============================================================================
StatusCode SherpaProduction::hadronize( HepMC::GenEvent *theEvent, 
                                        LHCb::GenCollision *theCollision) {
  debug() << "=== LbSherpa: hadronize" << endmsg;
  try {
    p_generator->GenerateOneEvent(true);
  }
  catch (ATOOLS::Exception Sherpaexception) {
    msg_Error()<<Sherpaexception<<std::endl;
    exit(1);
  }
  
  p_generator->FillHepMCEvent(*theEvent);
  
  // This converts GeV (Sherpa) to MeV (Gaudi)
  for ( HepMC::GenEvent::particle_iterator p = theEvent->particles_begin(); p != theEvent->particles_end(); ++p ) {
    HepMC::FourVector scaled_momentum((*p)->momentum().px() * Gaudi::Units::GeV,
                                      (*p)->momentum().py() * Gaudi::Units::GeV,
                                      (*p)->momentum().pz() * Gaudi::Units::GeV,
                                      (*p)->momentum().e()  * Gaudi::Units::GeV);
    (*p)->set_momentum(scaled_momentum);
    (*p)->set_generated_mass((*p)->generated_mass() * Gaudi::Units::GeV);
  }
  //Check Events
  //theEvent->print();

  hardProcessInfo(theCollision);
  return StatusCode::SUCCESS;
}

//=============================================================================
// Debug print out to be printed after all initializations
//=============================================================================
void SherpaProduction::printRunningConditions( ) { 
  debug() << "=== LbSherpa: printRunningConditions (NOT IMPLEMENTED)" << endmsg;
}

//=============================================================================
// Setup for forced fragmentation 
//=============================================================================
StatusCode SherpaProduction::setupForcedFragmentation( const int 
                                                       /*thePdgId*/ ) {
  debug() << "=== LbSherpa: setupForcedFragmentation (NOT IMPLEMENTED)" << endmsg;
  return StatusCode::SUCCESS;  
}

//=============================================================================
//interfacing the Gaudi RNG in sherpa                                           
//=============================================================================
using namespace ATOOLS;
// this makes LHCb_RNG loadable in Sherpa
DECLARE_GETTER(LHCb_RNG, "LHCb_RNG", External_RNG,RNG_Key);

External_RNG *ATOOLS::Getter<External_RNG,RNG_Key, LHCb_RNG>::operator()(const RNG_Key &) const {
  return new LHCb_RNG();
}

// this eventually prints a help message
void ATOOLS::Getter<External_RNG,RNG_Key,LHCb_RNG>::PrintInfo(std::ostream &str, const size_t) const {
  str<<"GaudiRandomGenerator interface";
}

double LHCb_RNG::Get(){
  return m_gaudiRNGProd();
}

// ============================================================================
/// The END 
// ============================================================================
