// $Header: /local/reps/lhcb/Gen/LbSherpa/src/SherpaProduction.h,v 1.1.1.1 2009/02/26 16:54:56 jwishahi Exp $
#ifndef LBSHERPA_SHERPAPRODUCTION_H 
#define LBSHERPA_SHERPAPRODUCTION_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Generators/IProductionTool.h"
#include "GaudiKernel/IParticlePropertySvc.h"
#include "SHERPA/Tools/Definitions.H"

// Forward declaration
class IBeamTool;

namespace SHERPA
{
  class Sherpa;
}

namespace HepMC
{
  class GenEvent;
}

/** @class SherpaProduction SherpaProduction.h 
 *  
 *  Interface tool to produce events with Sherpa
 * 
 *  @author Ramon Niet
 *  @date   2015-02-03
 */
class SherpaProduction : public GaudiTool, virtual public IProductionTool {
public:
  typedef std::vector<std::string> CommandVector;
  
  /// Standard constructor
  SherpaProduction( const std::string & type , const std::string & name ,
                     const IInterface * parent );
  
  virtual ~SherpaProduction( ); ///< Destructor
  
  virtual StatusCode initialize( );   ///< Initialize method
  
  virtual StatusCode finalize( );   ///< Finalize method
  
  /// Generate one event
  virtual StatusCode generateEvent( HepMC::GenEvent * theEvent , 
                                    LHCb::GenCollision * theCollision );

  virtual void hardProcessInfo( LHCb::GenCollision * theCollision );

  /// initialization specific to Sherpa
  virtual StatusCode initializeGenerator( );
  
  /// Set particle stable
  virtual void setStable( const LHCb::ParticleProperty * thePP );

  /// Modify particle property
  virtual void updateParticleProperties( const LHCb::ParticleProperty * thePP );

  /// Turn on fragmentation
  virtual void turnOnFragmentation( );

  /// Turn off fragmentation
  virtual void turnOffFragmentation( );

  /// Hadronize unfragmented event
  virtual StatusCode hadronize( HepMC::GenEvent * theEvent , 
                                LHCb::GenCollision * theCollision );
  
  /// Not needed in Sherpa
  virtual void savePartonEvent( HepMC::GenEvent * theEvent );

  /// Not needed in Sherpa
  virtual void retrievePartonEvent( HepMC::GenEvent * theEvent );

  /// Print parameters
  virtual void printRunningConditions( );

  /// Set particles not to update
  virtual bool isSpecialParticle( const LHCb::ParticleProperty * thePP ) const;

  /// Apply settings for forced fragmentation method
  virtual StatusCode setupForcedFragmentation( const int thePdgId );
  

protected:
  SHERPA::Sherpa *p_generator;
  int m_event_number;
  int m_warnings;
  bool m_initialised;
  
  std::map<std::string, std::string> m_parameters;

private:
  ATOOLS::Blob_List m_event;
  std::string m_beamToolName;
  IBeamTool* m_beamTool;
  bool m_doTheDecays;

  std::string m_RundatPath;
};

#include "ATOOLS/Math/Random.H"

class LHCb_RNG: public ATOOLS::External_RNG 
{
public:
  double Get();
};

#endif // LBSHERPA_SHERPAPRODUCTION_H

