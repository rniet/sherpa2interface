// $Header: /local/reps/lhcb/Gen/LbSherpa/src/SherpaDecay.cpp,v 1.3 2009/08/04 22:02:59 jwishahi Exp $

// header of this class

#include "SherpaDecay.h"

// from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "GaudiKernel/IParticlePropertySvc.h"
#include "GaudiKernel/ParticleProperty.h"
#include "GaudiKernel/GaudiException.h"

// from LHCb
#include "Kernel/ParticleID.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/PhysicalConstants.h"

// from GeneratorsModule
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "GaudiKernel/IRndmGenSvc.h"
#include "Generators/RandomForGenerator.h"
#include "GaudiKernel/RndmGenerators.h"

// from Event
#include "Event/HepMCEvent.h"

// from HepMC
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"
#include "HepMC/GenEvent.h"

// from Sherpa
#include "SHERPA/Main/Sherpa.H"
#include "SHERPA/Initialization/Initialization_Handler.H"
#include "SHERPA/Single_Events/Event_Handler.H"
#include "SHERPA/Single_Events/Hadron_Decays.H"
#include "SHERPA/SoftPhysics/Hadron_Decay_Handler.H"

#include "HADRONS++/Main/Hadron_Decay_Map.H"

#include "PHASIC++/Decays/Decay_Map.H"
#include "PHASIC++/Decays/Decay_Table.H"

#include "ATOOLS/Org/MyStrStream.H"

// other includes
#include <iostream>
#include <fstream>
#include <queue>

// from boost
#include "boost/filesystem.hpp"

using namespace SHERPA;
using namespace HepMC;

//-----------------------------------------------------------------------------
//  Implementation file for class: SherpaDecay
//
//  2015-02-03 Ramon Niet
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( SherpaDecay );

/// Random Generator, has to be global so that Sherpa can access it
Rndm::Numbers m_gaudiRNG;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SherpaDecay::SherpaDecay( const std::string& type,
                          const std::string& name,
                          const IInterface* parent )
  : GaudiTool ( type, name , parent ),
    p_sherpa(new Sherpa())
{
    declareInterface<IDecayTool>( this );
    debug() << "SherpaDecay::SherpaDecay" << std::endl;
    declareProperty( "RUNDATA", m_RundatPath = System::getEnv("SHERPADATFILESROOT")+"/SteeringDats/DecayToolDats/Run.dat", "Location of the SHERPA Run.dat steering file" );
    
    // obsolete as changed by SQL DB which holds decay tables in SHERPA
    // declareProperty( "DecaydatPath", m_DecaydatPath = "", "Location of the .dat decay files relative to the SHERPA_SHARE_PATH" ); 
    declareProperty( "SignalDecayNumber", m_SignalTableId = "11144103", "TableId of the decay we want to produce in generateSignalDecay, based on the LHCb process numbering scheme" );
    
    // For updates of the DDDB with Sherpa particle properties
    declareProperty( "CreatePPSvcTable", m_WriteppSvcTable = false, "Write ParticleProperty table with Sherpa values");


    debug() << " Called SherpaDecay " << endmsg;
}

//=============================================================================
// Destructor
//=============================================================================
SherpaDecay::~SherpaDecay( ) { }

//=============================================================================
// Initialize method
//=============================================================================
StatusCode SherpaDecay::initialize( ) {
    
  always() << "=============================================================" << endmsg;
  always() << " Using as decay engine " << this->type() << endmsg;
  always() << "=============================================================" << endmsg;
  
  // Initializing Gaudi tool
  StatusCode sc = GaudiTool::initialize( );
  if ( sc.isFailure() ) return sc;
  
  debug() << "SherpaDecay::initialize" << endmsg;
  info() << " Sherpa Run.dat path is     " << m_RundatPath << endmsg;
  info() << " Sherpa Decay directory is  " << m_DecaydatPath << endmsg;
  info() << " Sherpa SignalDecay ID is   " << m_SignalTableId << endmsg;
  
  // Setting Sherpa's Run.dat variables
  m_parameters["RUNDATA"]                = m_RundatPath;
  m_parameters["OUTPUT"]                 = "2"; //2:informational, 4:Tracking, 8:Debugging
  m_parameters["RESULT_DIRECTORY"]       = "/Results";
  m_parameters["EXTERNAL_RNG"]           = "LHCb_RNG_Decay";
  m_parameters["EVENTS"]                 = "1";
  m_parameters["PATH"]                   = System::getEnv("LBSHERPAROOT");
  // obsolete as changed by SQL DB which holds decay tables in SHERPA
  // m_parameters["DECAYPATH"]              = m_DecaydatPath.c_str();
  
  m_parameters["MASS_SMEARING"]          = "2";
  m_parameters["SOFT_SPIN_CORRELATIONS"] = "0";
  m_parameters["HARD_SPIN_CORRELATIONS"] = "0";
  m_parameters["SOFT_MASS_SMEARING"]     = "0";
  m_parameters["HARD_MASS_SMEARING"]     = "0";
    
  // Updating Sherpa kfTables with ParticlePropertyService
  LHCb::IParticlePropertySvc * ppSvc( 0 );
  try { ppSvc = svc< LHCb::IParticlePropertySvc > ( "LHCb::ParticlePropertySvc" , true ); }
  catch ( const GaudiException & exc ) {
    Exception( "Cannot open ParticlePropertySvc to update Sherpa masses" , exc );
  }


  // Initializing the random generator
  IRndmGenSvc* randSvc( 0 );
  try {
    randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true );
  } catch ( const GaudiException & exc ) {
    Exception( "RndmGenSvc not found to initialize Sherpa random engine" );
  }
  m_gaudiRNG.initialize( randSvc , Rndm::Flat( 0 , 1 ) );

    
  // Normal procedure to update particle properties in Sherpa, can be deactivated by setting m_WriteppSvcTable to true. The latter will create a ParticleProperty table with Sherpa properties.
  if (!m_WriteppSvcTable){
    for ( LHCb::IParticlePropertySvc::iterator i = ppSvc->begin(); i != ppSvc->end(); ++i ){
      unsigned long int pdg_id = abs((*i)->pdgID().pid());
      unsigned long int sherpa_id = ATOOLS::Flavour::PdgToSherpa(pdg_id);
      debug () << "trying to update pdg_id " << pdg_id << "; sherpa_id " << sherpa_id << std::endl;
      debug () << "... assigning mass " << (*i)->mass() << endmsg;
      m_parameters["MASS[" + ATOOLS::ToString(sherpa_id) + "]"] = ATOOLS::ToString((*i)->mass() / Gaudi::Units::GeV);
    }
  }
  release ( ppSvc );

  initializeGenerator();
    
  return StatusCode::SUCCESS; 
}

//=============================================================================
// Part specific to generator initialization
//=============================================================================
StatusCode SherpaDecay::initializeGenerator( ) {
  debug() << "SherpaDecay::initializeGenerator" << endmsg;
  // Setup sherpa arguments                                                      
  std::vector<std::string> argv;
  argv.push_back("dummy");
  for (std::map<std::string,std::string>::const_iterator it =m_parameters.begin(); it !=m_parameters.end(); ++it) {
    std::string command = (it->first + "=" + it->second);
    std::cout << command << std::endl;
    argv.push_back(command);
  }

  std::vector<char*> argc;
  for (std::vector<char*>::size_type i = 0; i != argv.size(); ++i) {
    argc.push_back( const_cast<char*>(argv[i].c_str() ) );
  }

  try {
    p_sherpa = new Sherpa();
    p_sherpa->InitializeTheRun(argc.size(), &argc.front());
    p_sherpa->InitializeTheEventHandler();
    HADRONS::Hadron_Decay_Map* hd_map = dynamic_cast<HADRONS::Hadron_Decay_Map*>(p_sherpa->GetInitHandler()->GetHDHandler()->DecayMap());
    hd_map->ReadFixedTables("/home/rniet/dbtests/Decaydata/", "FixedDecays.dat");
    // p_sherpa->SetTrials(1); //Doesnt exist in SHERPA 2.1.1
  }
  catch (ATOOLS::Exception Sherpaexception) {
    msg_Error()<<Sherpaexception<<std::endl;
    exit(1);
  }
  
  return StatusCode::SUCCESS;
}


//=============================================================================
// Finalize method
//=============================================================================
StatusCode SherpaDecay::finalize( ) {
  debug() << "SherpaDecay::finalize" << endmsg;
  
  m_gaudiRNG.finalize( );
  return GaudiTool::finalize( );

  //for Debug
  //return StatusCode::SUCCESS;
}


//=============================================================================
// Check if given Pdg Id is defined in the decay table
//=============================================================================
bool SherpaDecay::isKnownToDecayTool( int pdgId ) const {
  debug() << "SherpaDecay::isKnownToDecayTool" << endmsg;

  const unsigned int pdgId_abs = abs(pdgId);
  const bool anti = pdgId<0 ? true : false;

  if(unknownInStandardModelFlavourMap(pdgId_abs)) { // those particles are not part of the model used
    debug() << "SherpaDecay::isKnownToDecayTool skipping " << pdgId_abs << " (not part of model)" << endmsg;
    return false;
  }

  Decay_Handler_Base* hdh = p_sherpa->GetInitHandler()->GetHDHandler();

  if (hdh==NULL) {
    fatal() << "SherpaDecay::isKnownToDecayTool: THERE IS NO HADRON_DECAY_HANDLER" << endmsg;
    return false;
  }

  ATOOLS::Flavour particle(ATOOLS::Flavour::PdgToSherpa(pdgId_abs), anti);
  if(hdh->CanDecay(particle)) {
    debug() << "SherpaDecay::isKnownToDecayTool: yes " << particle <<  endmsg;
    return true;
  }
  debug() << "SherpaDecay::isKnownToDecayTool: no " << particle << endmsg;

  return false;
}


//=============================================================================
// Generate a decay tree from a particle theMother in the event theEvent
//=============================================================================
StatusCode SherpaDecay::generateDecay( HepMC::GenParticle * theMother ) const {
  debug() << "SherpaDecay::generateDecay" << endmsg;
  debug() << "SherpaDecay::generateDecay for " << theMother->pdg_id() << endmsg;
  
  HepMC::GenVertex * V = theMother -> production_vertex();
  Gaudi::LorentzVector theOriginPosition( V -> position() );

  bool anti = theMother->pdg_id()<0 ? true : false; 
  ATOOLS::Flavour mother_flav(ATOOLS::Flavour::PdgToSherpa(abs(theMother->pdg_id())), anti);
  debug() << "SherpaDecay::generateDecay asked to decay " << mother_flav << endmsg;

  if (mother_flav.IsStable()){
    debug() << "SherpaDecay::generateDecay particle " << mother_flav << " is stable, return SUCCESS " << endmsg;
    return StatusCode::SUCCESS;
  } else {
    debug() << "SherpaDecay::generateDecay particle " << mother_flav << " is instable, proceeding... " << endmsg;
  }

  ATOOLS::Vec4D mom(theMother->momentum().e()/Gaudi::Units::GeV,
                    theMother->momentum().px()/Gaudi::Units::GeV,
                    theMother->momentum().py()/Gaudi::Units::GeV,
                    theMother->momentum().pz()/Gaudi::Units::GeV);
  
  ATOOLS::Vec4D pos(theMother->production_vertex()->position().t(),
                    theMother->production_vertex()->position().x(),
                    theMother->production_vertex()->position().y(),
                    theMother->production_vertex()->position().z());

  ATOOLS::Blob_List* blobs = p_sherpa->GetEventHandler()->GetBlobs();

  ATOOLS::Particle* mother_in_part = new ATOOLS::Particle(1, mother_flav, mom);
  mother_in_part->SetTime();
  mother_in_part->SetFinalMass(mom.Mass());

  ATOOLS::Particle* mother_out_part = new ATOOLS::Particle(1, mother_flav, mom);
  mother_out_part->SetFinalMass(mom.Mass());

  ATOOLS::Blob* blob = blobs->AddBlob(ATOOLS::btp::Hadron_Decay);
  blob->SetPosition( pos );
  blob->SetStatus(ATOOLS::blob_status::needs_hadrondecays);
  blob->AddToInParticles(mother_in_part);
  blob->AddToOutParticles(mother_out_part);

  // verbose() << "SherpaDecay::generateDecay BLOB before decay:" << endmsg;
  // verbose() << *blob << endmsg;
  // verbose() << "SherpaDecay::generateDecay BLOBLIST before decay" << endmsg;
  // verbose() << *blobs << endmsg;
  // verbose() << "---------------------------------------------------------------------------" << endmsg;

  SHERPA::Decay_Handler_Base* hdh = p_sherpa->GetInitHandler()->GetHDHandler();
  SHERPA::Hadron_Decays* hd = new SHERPA::Hadron_Decays(hdh);
  double received_weight(1.);
  hd->Treat(blobs, received_weight);

  // verbose() << "SherpaDecay::generateDecay BLOB after decay:" << endmsg;
  // verbose() << *blob << endmsg;
  // verbose() << "SherpaDecay::generateDecay BLOBLIST after decay" << endmsg;
  // verbose() << *blobs << endmsg;
  // verbose() << "---------------------------------------------------------------------------" << endmsg;
  // verbose() << "---------------------------------------------------------------------------" << endmsg;
  // verbose() << "---------------------------------------------------------------------------" << endmsg;
  const StatusCode status_conversion = convertDecayTree(mother_out_part, theMother);
  blobs->Clear();
  if (status_conversion == StatusCode::SUCCESS){
    verbose() << "Particle after decay is " << theMother->pdg_id() << " with HepMCPrint" << endmsg;
    if(outputLevel() <= MSG::Level::VERBOSE){
      theMother->parent_event()->print();
    }
    theMother->set_status( LHCb::HepMCEvent::DecayedByDecayGenAndProducedByProdGen);
    return StatusCode::SUCCESS;
  }
  else{
    info() << "DecayTreeConversion failed" << endmsg;
    return StatusCode::FAILURE;
  }

}

//=============================================================================
//
//=============================================================================
StatusCode SherpaDecay::generateSignalDecay( HepMC::GenParticle * theMother , 
                                          bool & flip ) const
{
  debug() << "SherpaDecay::generateSignalDecay for:  " << theMother->pdg_id() << endmsg;
  // If particle already has daughters, return now
  if ( 0 != theMother->end_vertex() ){
    flip = false;
    warning() << "The particle has already been decayed (has endvertex)" << endmsg;
    return StatusCode::SUCCESS;
  }
  
  //Next decay shall be the signal decay defined in the decay table m_SignalTableId in the <DecayPath>/FixedDecays.dat
  HADRONS::Hadron_Decay_Map* hd_map = dynamic_cast<HADRONS::Hadron_Decay_Map*>(p_sherpa->GetInitHandler()->GetHDHandler()->DecayMap());
  if(hd_map){
    unsigned int mother_id = abs(theMother->pdg_id());
    ATOOLS::Flavour flav(ATOOLS::Flavour::PdgToSherpa(mother_id));
    PHASIC::Decay_Table* decaytable = hd_map->FindDecay(flav);
    if(decaytable){
      debug() << "DecayMap for the signal particle:" << endmsg;
      debug() << *decaytable << endmsg;

    } else {
      debug() << "No DecayTable found" << endmsg;
    }
    // hd_map->ReadFixedTables("/home/rniet/dbtests/Decaydata/", "FixedDecays.dat");
    hd_map->FixDecayTables(m_SignalTableId);
    decaytable = hd_map->FindDecay(flav);
    if(decaytable){
      debug() << "DecayMap for the signal particle after fixing:" << endmsg;
      debug() << *decaytable << endmsg;

    } else {
      debug() << "No DecayTable found" << endmsg;
    }
  }
  else {
    error() << "Failed to retrieve the DecayMap" << endmsg;
    return StatusCode::FAILURE;
  }
  
  if(SherpaDecay::generateDecay( theMother )){
    // Reset decay tables to normal
    hd_map->ClearFixedDecayTables();
    return StatusCode::SUCCESS;
  } else {
    std::cout << "NO SUCCESS in generateSignalDecay!!" << std::endl;
    hd_map->ClearFixedDecayTables();
    return StatusCode::FAILURE;
  }

}

//=============================================================================
//
//=============================================================================
StatusCode SherpaDecay::generateDecayWithLimit(HepMC::GenParticle * theMother, int targetId ) const {
  //DEBUG-JW: Have a look at Decaywithlimit
  debug() << "SherpaDecay::generateDecayWithLimit called. Motherparticle is " << theMother->pdg_id() << " and TargetId is " << targetId << "." << endmsg;
  
  if ( targetId != 0 ){
    Decay_Handler_Base* hdh = p_sherpa->GetInitHandler()->GetHDHandler();
  
    std::vector<int> KfTmpStbl; // Kfcodes of particles that are temporarily set stable are saved in here to undo the changes
  
    //Check for mass of PDGId=targetID and set all particles with mass<mass(targetId) to stable if they weren't stable before
    unsigned int targetId_abs = abs(targetId);
    ATOOLS::Flavour targetflav(ATOOLS::Flavour::PdgToSherpa(targetId_abs));
    debug () << "SherpaDecay::generateDecayWithLimit targetflav is " << targetflav << endmsg;
  
    // Debug RN: print current flavour map
    // for ( auto kfit : ATOOLS::s_kftable ){
    //   ATOOLS::Flavour tableflav( kfit.first );
    //   debug () << "SherpaDecay::generateDecayWithLimit Particle: " << tableflav << endmsg;
    //   debug () << "SherpaDecay::generateDecayWithLimit isStable: " << kfit.second->m_stable << endmsg;
    // }

    for ( auto kfit : ATOOLS::s_kftable ){
      ATOOLS::Flavour tableflav( kfit.first );
      if ( tableflav.IsHadron() && tableflav.HadMass() <= targetflav.HadMass() ){
        if ( hdh->CanDecay(tableflav.Kfcode() ) && !tableflav.IsStable() ){
          tableflav.SetStable(1);
          KfTmpStbl.push_back(tableflav.Kfcode()); //remember this code
        }
      }
    }

    // Debug RN: prints how the decay has been performed 
    // debug () << "SherpaDecay::generateDecayWithLimit theMother before decay: " << endmsg;
    // theMother->production_vertex()->print();
    
     SherpaDecay::generateDecay( theMother );
    
    // debug () << "SherpaDecay::generateDecayWithLimit theMother after decay: " << endmsg;

    // theMother->production_vertex()->print();
    // std::queue<HepMC::GenVertex*> decay_vertices;
    
    // if(theMother->end_vertex())
    //   decay_vertices.push(theMother->end_vertex());

    // while(!decay_vertices.empty()) {
    //   decay_vertices.front()->print();      
    //   for(HepMC::GenVertex::particles_out_const_iterator it = decay_vertices.front()->particles_out_const_begin();
    //       it != decay_vertices.front()->particles_out_const_end();
    //       it++) {
    //     if((*it)->end_vertex())
    //       decay_vertices.push((*it)->end_vertex());
    //   }
    //   decay_vertices.pop();
    // }
  
    // Reset the temporary stable particles
    for ( auto j : KfTmpStbl ){
      ATOOLS::Flavour tableflav(j);
      tableflav.SetStable(0);
    }

  } else {
    warning() << "SherpaDecay::generateDecayWithLimit No targetId specified, performing decay without limit." << endmsg;
    generateDecay( theMother);
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//
//=============================================================================
bool SherpaDecay::convertDecayTree(ATOOLS::Particle* parton, GenParticle* theMother) const
{
  //debug() << "SherpaDecay::convertDecayTree" << endmsg;
  ATOOLS::Blob *dc(parton->DecayBlob());

  if (dc==NULL) {
    //debug() << "SherpaDecay::convertDecayTree dc is NULL" << endmsg;
    theMother->set_status(999);
    return true;
  }

  ATOOLS::Vec4D pos(dc->Position());
  HepMC::FourVector position(pos[1],pos[2],pos[3],pos[0]);        //<- Right units?
  HepMC::GenVertex *vertex(new HepMC::GenVertex(position));
  vertex->weights().push_back(1.0);
  vertex->add_particle_in(theMother);
  theMother->parent_event()->add_vertex(vertex);
  HepMC::GenParticle *daughter(NULL);
  //debug() << "SherpaDecay::convertDecayTree now iterating daughters" << endmsg;
  for (int i(0);i<dc->NOutP();++i) {
    if (sherpa2HepMC(dc->OutParticle(i),daughter)) {
      vertex->add_particle_out(daughter);
      if (!convertDecayTree(dc->OutParticle(i),daughter)) return false;
    }
    else {
      error() << __FUNCTION__<< "(): Error during conversion." << endmsg;
      return false;
    }
  }
 
  ATOOLS::Vec4D insum, outsum;
  for (int i(0);i<dc->NInP();++i) insum+=dc->InParticle(i)->Momentum();
  for (int i(0);i<dc->NOutP();++i) outsum+=dc->OutParticle(i)->Momentum();
  if (ATOOLS::dabs(1.- vertex->check_momentum_conservation()/(outsum-insum).PSpat()/1000.)>1e-6 &&
      outsum!=insum) {
    debug() << __FUNCTION__ << "(): Momentum not conserved. Continue.\n"
             << vertex->check_momentum_conservation() << " <- "
             << (outsum-insum) << "\n" << *dc << std::endl;
  }
   
  return true;
  
}

//=============================================================================
//
//=============================================================================
bool SherpaDecay::sherpa2HepMC(ATOOLS::Particle * parton, HepMC::GenParticle *& particle) const
{
  ATOOLS::Vec4D mom  = 1000.*parton->Momentum();
  HepMC::FourVector momentum(mom[1],mom[2],mom[3],mom[0]);
  int stat = parton->Status();                             // int -> part_status::code
  if (parton->DecayBlob()!=NULL) stat = 777;
  else if (stat==1) stat=999;
  particle = new GenParticle(momentum,parton->Flav(),stat);
  return true;
}

bool SherpaDecay::unknownInStandardModelFlavourMap(unsigned int pdgId) const{
  switch(pdgId){
    case 7:
    case 8:
    case 17:
    case 18:
    case 32:
    case 33:
    case 34:
    case 35:
    case 36:
    case 37:
    case 39:
    case 41:
    case 42:
    case 43:
    case 44:
    case 81:
    case 82:
    case 83:
    case 84:
    case 85:
    case 86:
    case 87:
    case 88:
    case 150:
    case 350:
    case 510:
    case 530:
    case 1112:
    case 1116:
    case 1118:
    case 1212:
    case 1214:
    case 1216:
    case 1218:
    case 2116:
    case 2118:
    case 2122:
    case 2124:
    case 2126:
    case 2128:
    case 2216:
    case 2218:
    case 2222:
    case 2226:
    case 2228:
    case 3116:
    case 3118:
    case 3124:
    case 3126:
    case 3128:
    case 3216:
    case 3218:
    case 3226:
    case 3228:
    case 5124:
    case 10022:
    case 11112:
    case 11114:
    case 11116:
    case 11212:
    case 11216:
    case 12112:
    case 12114:
    case 12116:
    case 12118:
    case 12122:
    case 12126:
    case 12212:
    case 12214:
    case 12216:
    case 12218:
    case 12222:
    case 12224:
    case 12226:
    case 13112:
    case 13114:
    case 13116:
    case 13122:
    case 13124:
    case 13126:
    case 13212:
    case 13214:
    case 13216:
    case 13222:
    case 13224:
    case 13226:
    case 13314:
    case 13324:
    case 14122:
    case 15122:
    case 20022:
    case 21112:
    case 21114:
    case 21212:
    case 21214:
    case 22112:
    case 22114:
    case 22122:
    case 22124:
    case 22212:
    case 22214:
    case 22222:
    case 22224:
    case 23112:
    case 23114:
    case 23122:
    case 23124:
    case 23126:
    case 23212:
    case 23214:
    case 23222:
    case 23224:
    case 30343:
    case 30353:
    case 30363:
    case 31114:
    case 31214:
    case 32112:
    case 32114:
    case 32124:
    case 32212:
    case 32214:
    case 32224:
    case 33122:
    case 41214:
    case 42112:
    case 42124:
    case 42212:
    case 43122:
    case 52114:
    case 52214:
    case 53122:
    case 100315:
    case 100325:
    case 100411:
    case 100413:
    case 100421:
    case 100423:
    case 103316:
    case 103326:
    case 104124:
    case 104312:
    case 104314:
    case 104322:
    case 104324:
    case 203316:
    case 203326:
    case 203338:
    case 204126:
    case 1000001:
    case 1000002:
    case 1000003:
    case 1000004:
    case 1000005:
    case 1000006:
    case 1000011:
    case 1000012:
    case 1000013:
    case 1000014:
    case 1000015:
    case 1000016:
    case 1000021:
    case 1000022:
    case 1000023:
    case 1000024:
    case 1000025:
    case 1000035:
    case 1000037:
    case 1000039:
    case 2000001:
    case 2000002:
    case 2000003:
    case 2000004:
    case 2000005:
    case 2000006:
    case 2000011:
    case 2000012:
    case 2000013:
    case 2000014:
    case 2000015:
    case 2000016:
    case 3000111:
    case 3000113:
    case 3000211:
    case 3000213:
    case 3000221:
    case 3000223:
    case 3000331:
    case 3100021:
    case 3100111:
    case 3100113:
    case 3200111:
    case 3200113:
    case 3300113:
    case 3400113:
    case 4000001:
    case 4000002:
    case 4000011:
    case 4000012:
    case 5000039:
    case 9042413:
    case 9900012:
    case 9900014:
    case 9900016:
    case 9900023:
    case 9900024:
    case 9900041:
    case 9900042:
    case 9910445:
    case 9920443:
    case 99000000:
    case 480000000:
    case 1000010020:
    case 1000010030:
    case 1000020030:
    case 1000020040:
    case 1000030070:
    case 1000040080:
    case 1000040090:
    case 1000040100:
    case 1000050100:
    case 1000050110:
    case 1000050120:
    case 1000060120:
    case 1000060130:
    case 1000060140:
    case 1000070140:
    case 1000070150:
    case 1000070160:
    case 1000080160:
    case 1000080170:
    case 1000080180:
    case 1000080190:
    case 1000090190:
    case 1000100220:
    case 1000100230:
    case 1000110240:
    case 1000120240:
    case 1000120250:
    case 1000120260:
    case 1000120270:
    case 1000130270:
    case 1000130280:
    case 1000140280:
    case 1000140290:
    case 1000140300:
    case 1000150310:
    case 1000170390:
    case 1000170400:
    case 1000180360:
    case 1000180400:
    case 1000240500:
    case 1000240520:
    case 1000240530:
    case 1000240540:
    case 1000250550:
    case 1000260540:
    case 1000260560:
    case 1000260570:
    case 1000260590:
    case 1000280580:
    case 1000280600:
    case 1000280610:
    case 1000280620:
    case 1000280630:
    case 1000280640:
    case 1000290630:
    case 1000290650:
    case 1000420920:
    case 1000420950:
    case 1000420960:
    case 1000420970:
    case 1000420980:
    case 1000421000:
    case 1000461080:
    case 1000791970:
    case 1000822040:
    case 1000822060:
    case 1000822070:
    case 1000822080:
      return true;
    default:
      return false;
  }
}

//=============================================================================
//interfacing the Gaudi RNG in sherpa                                           
//=============================================================================
using namespace ATOOLS;
// this makes LHCb_RNG loadable in Sherpa
DECLARE_GETTER(LHCb_RNG_Decay, "LHCb_RNG_Decay", External_RNG,RNG_Key);

External_RNG *ATOOLS::Getter<External_RNG,RNG_Key, LHCb_RNG_Decay>::operator()(const RNG_Key &) const {
  return new LHCb_RNG_Decay();
}

// this eventually prints a help message
void ATOOLS::Getter<External_RNG,RNG_Key,LHCb_RNG_Decay>::PrintInfo(std::ostream &str, const size_t) const {
  str<<"GaudiRandomGenerator interface";
}

double LHCb_RNG_Decay::Get(){
  return m_gaudiRNG();
}

// ============================================================================
/// The END 
// ============================================================================
