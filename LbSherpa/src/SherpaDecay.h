// $Header: /local/reps/lhcb/Gen/LbSherpa/src/SherpaDecay.h,v 1.1.1.1 2009/02/26 16:54:56 jwishahi Exp $
#ifndef LBSHERPA_SHERPADECAY_H 
#define LBSHERPA_SHERPADECAY_H 1

// Include files
// from STL
#include <string>

// from boost
#include "boost/filesystem/path.hpp"

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Vector4DTypes.h"


// from GeneratorModules
#include "MCInterfaces/IDecayTool.h"

// forward declarations
namespace LHCb {
  class ParticleID;
}

namespace SHERPA {
  class HepMC_Interface;
  class Sherpa;
}
namespace ATOOLS {
  class Blob_List;
  class Particle;
}

/** @class SherpaDecay SherpaDecay.h "SherpaDecay.h"
  * 
  * Main algorithm for the SherpaDecayTool.
  *
  * @author  Ramon Niet
  * @date    2015-02-03
  */


class SherpaDecay : public GaudiTool, virtual public IDecayTool {
public:
  /// Standard constructor
      SherpaDecay( const std::string& type, const std::string& name, const IInterface* parent);
  
      virtual ~SherpaDecay( ); ///< Destructor
    
  /// Initialize method.
      virtual StatusCode initialize();
          
  /// Method to initialize the Sherpa generator similar to method in production tool
      StatusCode initializeGenerator();

  /// Finalize method
      virtual StatusCode finalize();

  /// Implements IDecayTool::generateDecay
      virtual StatusCode generateDecay( HepMC::GenParticle * theMother ) const;

  /// Implements IDecayTool::generateSignalDecay
      virtual StatusCode generateSignalDecay( HepMC::GenParticle * theMother , bool & flip ) const;

      //Not needed?? void checkParticle( const HepMC::GenParticle * thePart ) const;
  
  /// Implements IDecayTool::generateDecayWithLimit
      virtual StatusCode generateDecayWithLimit( HepMC::GenParticle * theMother,
                                             int targetId ) const;
  /// Implements IDecayTool::enableFlip
      virtual void enableFlip() const { return; };

  /// Implements IDecayTool::disableFlip
      virtual void disableFlip() const { return; };
  
  /// Implements IDecayTool::isKnownToDecayTool
      virtual bool isKnownToDecayTool( int pdgId ) const ;

  /// Implements IDecayTool::getSignalBr
      virtual double getSignalBr( ) const { return 1.; } 
  
  /// Implements IDecayTool::checkSignalPresence
      virtual bool checkSignalPresence( ) const { return true; } 

  /// Implements IDecayTool::setSignal
      virtual void setSignal( const int ) { return; }

  /// SHERPA would SegFault asking the DecayTable for pdgIds not contained in the model
      bool unknownInStandardModelFlavourMap(unsigned int pdgid) const;

protected:
  int m_event_number;
  int m_warnings;
  
  std::map<std::string, std::string> m_parameters;

  void cleanupSherpaEvent(ATOOLS::Blob_List* blobs) const;
  bool convertDecayTree(ATOOLS::Particle* parton, HepMC::GenParticle* particle) const;
  bool sherpa2HepMC(ATOOLS::Particle * parton, HepMC::GenParticle *& particle) const;
  
private:
  SHERPA::Sherpa* p_sherpa;
  
  std::string m_RundatPath;
  std::string m_DecaydatPath;
  std::string m_SignalTableId;
  bool m_WriteppSvcTable;
};


#include "ATOOLS/Math/Random.H"
class LHCb_RNG_Decay: public ATOOLS::External_RNG 
{
public:
  double Get();
};


#endif // LBSHERPA_SHERPADECAY_H
