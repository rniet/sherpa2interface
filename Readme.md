# Installation auf Lxplus

LbLogin Skript ausführen:

    source /afs/cern.ch/lhcb/software/releases/LBSCRIPTS/LBSCRIPTS_v8r3/InstallArea/scripts/LbLogin.sh
    
LHCb Simulation Software aufsetzen:

    SetupProject Gauss v48r0 --build-env
    getpack -p anonymous Gen/LbSherpa
    getpack -p anonymous Gen/SherpaDatFiles
    getpack -p anonymous Gen/sherpa

Anschließend im sherpa2interface git repo in der install_lxplus.sh
die Pfade zum cmtuser Verzeichnis sowie zum sherpa2interface repo anpassen.
Dann sourcen:

    source install_lxplus.sh

Dann kann man im cmtuser directory in die einzelnen Teilpakete wechseln und
das interface bauen:

    cd Gen/sherpa/cmt
    cmt make -j
    cd Gen/SherpaDatFiles/cmt/
    cmt make -j
    cd Gen/LbSherpa/cmt/
    cmt config
    source setup.sh
    cmt make -j

LHCb Software zum Einsatz bereit machen:

    SetupProject Gauss v48r0

Anschließend in Testruns eine der Konfigurationen probieren:

    gaudirun.py *.py
